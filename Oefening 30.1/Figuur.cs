﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30._1
{
    class Figuur : IObject
    {
        private double _breedte;
        private double _lengte;

        public Figuur() { }
        public Figuur(double lengte, double breedte)
        {
            this.Lengte = lengte;
            this.Breedte = breedte;
        }

        public double Breedte
        {
            get { return _breedte; }
            set { _breedte = value; }
        }
        public double Lengte
        {
            get { return _lengte; }
            set { _lengte = value; }
        }

        public override string ToString()
        {
            return $"De oppervlakte van de figuur met lengte {Lengte}cm en breedte {Breedte}cm is {Math.Round(Bereken(), 2)}cm².";
        }
        public double Bereken()
        {
            return Lengte * Breedte;
        }
    }
}
