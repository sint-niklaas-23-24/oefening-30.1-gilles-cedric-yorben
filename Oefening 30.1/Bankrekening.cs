﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30._1
{
    class Bankrekening : IObject
    {
        private double _rentevoet;
        private double _saldo;

        public Bankrekening() { }
        public Bankrekening(double saldo, double  rentevoet) 
        {
            Saldo = saldo;
            Rentevoet = rentevoet;
        }

        public double Rentevoet
        {
            get { return _rentevoet; }
            set { _rentevoet = value; }
        }
        public double Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }

        public override string ToString()
        {
            return $"De interest van de rekening met saldo {Saldo}€ en rentevoet {Rentevoet}% is {Math.Round(Bereken(), 2)}€";
        }
        public double Bereken()
        {
            return Saldo * (Rentevoet / 100 + 1) - Saldo;
        }
    }
}
