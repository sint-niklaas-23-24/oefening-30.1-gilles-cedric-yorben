﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_30._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Bankrekening b1;
        Figuur f1;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void btbBerekenIntrest_Click(object sender, RoutedEventArgs e)
        {
            ShowMessage(b1 = new Bankrekening(1000, 2.3));
        }

        private void btnBerekenOppervlakte_Click(object sender, RoutedEventArgs e)
        {
            ShowMessage(f1 = new Figuur(10, 30));
        }

        public void ShowMessage(IObject obj)
        {
            MessageBox.Show(obj.ToString(), "Informatie", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
